package com.lululemon.hackathon;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class HelloWorldController {

    @GetMapping(value = "/hello-world/{input}")
    public ResponseEntity<String> helloWorld(@PathVariable("input") String requestString) {
        log.info("User input :{}", requestString);
        return new ResponseEntity<>("Hello World :" + requestString, HttpStatus.OK);
    }
}
