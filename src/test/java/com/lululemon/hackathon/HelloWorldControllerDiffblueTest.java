package com.lululemon.hackathon;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {HelloWorldController.class})
@ExtendWith(SpringExtension.class)
class HelloWorldControllerDiffblueTest {
    @Autowired
    private HelloWorldController helloWorldController;

    /**
     * Method under test: {@link HelloWorldController#helloWorld(String)}
     */
    @Test
    void testHelloWorld() throws Exception {
        // Arrange
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/hello-world/{input}", "Input");

        // Act and Assert
        MockMvcBuilders.standaloneSetup(helloWorldController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(MockMvcResultMatchers.content().string("Hello World :Input"));
    }

    /**
     * Method under test: {@link HelloWorldController#helloWorld(String)}
     */
    @Test
    void testHelloWorld2() throws Exception {
        // Arrange
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/hello-world/{input}", "Uri Variables",
                                                                                  "Uri Variables");

        // Act and Assert
        MockMvcBuilders.standaloneSetup(helloWorldController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(MockMvcResultMatchers.content().string("Hello World :Uri Variables"));
    }
}
